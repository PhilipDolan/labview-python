# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Main control software for ROADMap stacked WSS characterisation ###

* Controls and synchronises the following equipment using LabVIEW and python
Yenista (now EXFO) TUNICS T100 			https://yenista.com/Tunable-Lasers,7.html 
Yenista CT400							https://yenista.com/Component-Tester,9.html
Thorlabs Ge PDA 						https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=947
PicoLogger								https://www.picotech.com/data-logger/picolog-1000-series/multi-channel-daq
Laser Components 1x16 					https://www.lasercomponents.com/uk/product/optical-switches-single-mode/

### How do I get set up? ###
See hand over notes for how to start and run software