##############################################################################
# Import some libraries
##############################################################################
import sys
import os
import glob
import time
import re
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import scipy.optimize as opt
import socket
import scipy.io as io
import importlib.util
import ntpath

from scipy.interpolate import RectBivariateSpline
from scipy.interpolate import interp1d
from scipy.signal import find_peaks_cwt
from scipy.ndimage.filters import gaussian_filter
from mpl_toolkits.mplot3d import Axes3D
from scipy.signal import savgol_filter


##############################################################################
# Import some extra special libraries from my own repo
##############################################################################
sys.path.insert(0, r'C:\Users\Philip\Documents\Python\Local Repo\library')
import useful_defs_prd as prd
from peakdetect import peakdetect

##############################################################################
# Do some stuff
##############################################################################

cs = prd.palette()
f1 = r'..\..\Data\Pico Log\*.csv'
files = glob.glob(f1)
print(files)

fig1 = plt.figure('fig1')
fig1.patch.set_facecolor(cs['mdk_dgrey'])

all_data = np.array([])
for i1 in files:
    my_data = np.genfromtxt(i1, delimiter=',')

    all_data = np.append(all_data, my_data)

plt.plot(all_data,':.')
plt.show()
