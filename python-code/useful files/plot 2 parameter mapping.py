##############################################################################
# Import some libraries
##############################################################################
import sys
import os
import glob
import time
import re
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import scipy.optimize as opt
import socket
import scipy.io as io
import importlib.util
import ntpath

from scipy.interpolate import RectBivariateSpline
from scipy.interpolate import interp1d
from scipy.signal import find_peaks_cwt
from scipy.ndimage.filters import gaussian_filter
from mpl_toolkits.mplot3d import Axes3D
from scipy.signal import savgol_filter


##############################################################################
# Import some extra special libraries from my own repo
##############################################################################
sys.path.insert(0, r'C:\Users\User\Documents\GitHub\python-resources\library')
import useful_defs_prd as prd
from peakdetect import peakdetect
cs = prd.palette()

##############################################################################
# Do some stuff
##############################################################################
p0 = r"C:\Users\User\Documents\Phils LabVIEW\Data\Parameter maps\P0.csv"
p1 = r"C:\Users\User\Documents\Phils LabVIEW\Data\Parameter maps\P1.csv"
p2 = r"C:\Users\User\Documents\Phils LabVIEW\Data\Parameter maps\CT400.csv"
p3 = r"C:\Users\User\Documents\Phils LabVIEW\Data\Parameter maps\PicoLog.csv"

d0 = np.genfromtxt(p0, delimiter=',')
d1 = np.genfromtxt(p1, delimiter=',')
d2 = np.genfromtxt(p2, delimiter=',')
d3 = np.genfromtxt(p3, delimiter=',')

# ##############################################################################
# # Plot some figures
# ##############################################################################
print(d0)
print(d1)
print(d2)
print(d3)
x = d1[0, :]
y = d0[:, 0]

fig1 = plt.figure('fig1')
ax1 = fig1.add_subplot(1, 1, 1)
fig1.patch.set_facecolor(cs['mdk_dgrey'])
ax1.set_xlabel('P1 - sin(ϕ) offset')
ax1.set_ylabel('P0 - sin(ϕ) amplitude')
plt.imshow(d3, aspect='auto', interpolation='none',
           extent=prd.extents(x) + prd.extents(y), origin='lower')
plt.colorbar()
plt.show()

prd.PPT_save_2d(fig1, ax1, 'figure1')
