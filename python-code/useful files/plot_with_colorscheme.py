##############################################################################
# Import some libraries
##############################################################################

import os
import glob
import time
import re
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import scipy.optimize as opt
import socket
import scipy.io as io
import LabVIEW_hologen as Lab
import importlib.util

from scipy.interpolate import interp1d
from scipy.signal import find_peaks_cwt
from scipy.ndimage.filters import gaussian_filter
from mpl_toolkits.mplot3d import Axes3D
from scipy.signal import savgol_filter
from peakdetect import peakdetect

##############################################################################
# Set plotting colours and style
##############################################################################

spec = importlib.util.spec_from_file_location("module.name", "/path/to/file.py")
foo = importlib.util.module_from_spec(spec)
spec.loader.exec_module(foo)
foo.MyClass()

##############################################################################
# Set plotting colours and style
##############################################################################

mdk_purple = [115 / 255, 90 / 255, 158 / 255]
mdk_dgrey = [39 / 255, 40 / 255, 34 / 255]
mdk_lgrey = [96 / 255, 96 / 255, 84 / 255]
mdk_green = [95 / 255, 164 / 255, 44 / 255]
mdk_yellow = [229 / 255, 220 / 255, 90 / 255]
mdk_blue = [75 / 255, 179 / 255, 232 / 255]
mdk_orange = [224 / 255, 134 / 255, 31 / 255]
mdk_pink = [180 / 255, 38 / 255, 86 / 255]

plt.style.use('ggplot')
plt.rcParams['font.size'] = 8
plt.rcParams['font.family'] = 'monospace'
plt.rcParams['font.fantasy'] = 'Nimbus Mono'
plt.rcParams['axes.labelsize'] = 8
plt.rcParams['axes.labelweight'] = 'normal'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 10
plt.rcParams['lines.color'] = 'white'
plt.rcParams['text.color'] = mdk_yellow
plt.rcParams['axes.labelcolor'] = mdk_yellow
plt.rcParams['xtick.color'] = mdk_purple
plt.rcParams['ytick.color'] = mdk_purple
plt.rcParams['axes.edgecolor'] = mdk_lgrey
plt.rcParams['savefig.edgecolor'] = mdk_lgrey
plt.rcParams['axes.facecolor'] = mdk_dgrey
plt.rcParams['savefig.facecolor'] = mdk_dgrey
plt.rcParams['grid.color'] = mdk_lgrey
plt.rcParams['grid.linestyle'] = ':'

##############################################################################
# Do some stuff
##############################################################################

files = glob.glob(
    r'C:\Users\Philip\Documents\Data\Phase response\0 ph_lwlim\*.csv')
    # r'C:\Users\Philip\Documents\Data\Phase response\csvs\*.csv')

for i1 in files[0:]:
    temp_data = np.genfromtxt(i1, delimiter=',')
    filename = str(i1[0:-4])
    temp_data = temp_data[1]
    mean_max = np.mean(temp_data[-10000:-1])

##############################################################################
    # Do a first ROI identification using a running mean & a threshold
    temp_rmean = Lab.Running_mean(temp_data, 500)

    thresh_ROI1 = 0.9 * mean_max
    for i1, i2 in enumerate(temp_rmean):

        if i2 < thresh_ROI1:
            start_ROI1 = i1 - 1000
            break

    for i1, i2 in enumerate(np.flipud(temp_rmean)):

        if i2 < thresh_ROI1:
            end_ROI1 = len(temp_rmean) - i1 + 1000
            break

    ROI1 = temp_data[end_ROI1 - 6000:end_ROI1 - 500]

##############################################################################
    # Gauss filter to smooth data in ROI1
    gauss_ROI1 = gaussian_filter(ROI1, sigma=51)
    # Find the derivative of the smoothed data
    grad_ROI1 = np.gradient(gauss_ROI1)
    # Normalise the gradient
    grad_ROI1 = grad_ROI1 / max(grad_ROI1)
    # Detect peaks in the data
    # grad_peaks_out = peakdetect(ROI1, lookahead=4000)
    # Detect peaks in the derivative
    grad_peaks_out = peakdetect(grad_ROI1, lookahead=4000)

##############################################################################
    # Do a second ROI identification from the smoothed derivative of the data
    thresh_grad = 2 * gauss_ROI1[0]
    for i1, i2 in enumerate(grad_ROI1):

        if i2 > thresh_grad:
            start_ROI2 = i1
            break

    ROI2 = ROI1[start_ROI2:]
    ROI2 = ROI2 / max(ROI2) - min(ROI2)

    # find zero crossings of derivative of peaks (kinda like peak finding...)
    # zero_crossings = np.where(np.diff(np.sign(grad_ROI)))[0]

    # fft of data
    # data_fft = np.fft.fft(temp_data)
    # window the fft data
    # clipped_fft = data_fft[0:500]
    # ifft to get a smoothed data set
    # ifft_data = np.fft.ifft(clipped_fft)

##############################################################################
    # do the curve fit for the phase mapping
    x0 = np.linspace(0, 255, len(ROI2))
    x1 = np.linspace(0, 255, 25)

    initial_guess = (5, 1 / 100)
    f1 = interp1d(x0, ROI2)

    try:
        popt, pcov = opt.curve_fit(
            Lab.Phase, x0, ROI2, p0=initial_guess,
            bounds=([0, -np.inf], [np.inf, np.inf]))

    except RuntimeError:
        print("Error - curve_fit failed")

    fig1 = plt.figure('fig1')
    ax1 = fig1.add_subplot(111)
    fig1.patch.set_facecolor(mdk_dgrey)
    fig1.patch.set_alpha(1)
    plt.plot(temp_data, '.', c=mdk_yellow)
    plt.plot(temp_rmean, c=mdk_blue)
    plt.savefig(filename + 'data.png')
    plt.cla()	

    # fig2 = plt.figure('fig2')
    # fig2.patch.set_facecolor(mdk_dgrey)
    # fig2.patch.set_alpha(1)
    # plt.plot(ROI_time, ROI, '.', mfc=mdk_orange)
    # plt.plot(ROI_time, gauss_ROI, c=mdk_yellow)
    # plt.plot(ROI_time, grad_ROI, c=mdk_green)

    fig3 = plt.figure('fig3')
    fig3.patch.set_facecolor(mdk_dgrey)
    fig3.patch.set_alpha(1)
    plt.plot(x0, ROI2, ':')
    plt.plot(x1, f1(x1), '.')
    plt.plot(x0, Lab.Phase(x0, *popt), '-', c=mdk_green)
    plt.xlabel('time')
    plt.ylabel('detector power')
    plt.savefig(filename + 'ROI.png')
    plt.cla()	
    x3 = range(255)
    ϕ_A = popt[0]
    ϕ_B = popt[1]
    ϕ_g = (2 / np.pi) * np.abs(ϕ_A) * (1 - np.exp(-ϕ_B * x3))
    # return (ϕ_A, ϕ_B, ϕ_g)

    g_ϕ = interp1d(ϕ_g, range(255))
    ϕ_min = min(ϕ_g)
    ϕ_max = max(ϕ_g)

    fig4 = plt.figure('fig4')
    fig4.patch.set_facecolor(mdk_dgrey)
    fig4.patch.set_alpha(1)
    plt.plot(ϕ_g)

plt.figure('fig4')
plt.xlabel('grey level')
plt.ylabel('phase')
plt.show()
