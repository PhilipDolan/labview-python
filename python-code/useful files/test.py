import numpy as np

a = np.array([1, 2, 3])
b = np.array([4, 5, 6])
c = np.column_stack((a, b))
print(c)

np.savetxt('test.csv', c, delimiter=',')
