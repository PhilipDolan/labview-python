##############################################################################
# Import some libraries
##############################################################################

import os
import glob
import time
import re
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import scipy.optimize as opt
import socket
import scipy.io as io
import LabVIEW_hologen as Lab

from scipy.interpolate import interp1d
from scipy.signal import find_peaks_cwt
from scipy.ndimage.filters import gaussian_filter
from mpl_toolkits.mplot3d import Axes3D
from scipy.signal import savgol_filter
from peakdetect import peakdetect

##############################################################################
# Do some stuff
##############################################################################
x = np.array([1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6])
y = np.flipud(x)
z = 2 * x

fig3 = plt.figure(3)

for i1 in range(10):
    print(i1)
    fig1 = plt.figure(1)
    plt.plot(x + i1)
    plt.draw()
    plt.figure(3)
    plt.plot(x + i1*i1)
    plt.show(3)



fig2 = plt.figure(2)
plt.plot(y)
plt.show(2)
